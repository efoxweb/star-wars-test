function getData(endpoint, callback) {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', endpoint);
    xhr.responseType = 'json';
    xhr.send();
    xhr.onload = function() {
        let response = xhr.response;
        callback(response);
    };
}

function getPagedData(endpoint, results, callback) {
    getData(endpoint, function(response) {
        results = _.concat(results, response.results);
        if (response.next) {
            getPagedData(response.next, results, callback);
        } else {
            callback(results);
        }
    });
}

function getStarships(callback) {
    getPagedData('https://swapi.co/api/starships/', [], function(results) {
        callback(results);
    });
}

function getFilms(films = [], callback, index = 0, filmArr = []) {

    if (index === films.length) {
        callback(filmArr);
        return;
    }

    let filmEndpoint = films[index];

    getData(filmEndpoint, function (response) {
        filmArr[filmEndpoint] =
            {
                title: response.title,
                director: response.director,
                release_date: response.release_date
            };

        getFilms(films, callback, index+1, filmArr);

    });
}

function sortOrder(json, callback) {
    json.result = _.sortBy(json.result, [function(o) { return parseInt(o.crew); }]);
    json.result = _.remove(json.result, function(n) {
        return n.crew < 10 == 0;
    });
    json.count = json.result.length;
    callback(json);
}

function formatJson(callback) {
    getStarships(function (results) {
        let json = {},
            filmsAppeared = [];
        json.count = results.length;
        json.result = [];

        results.forEach((result, index) => {
            json.result[index] = {
                name: result.name,
                model: result.model,
                crew: result.crew,
                passengers: result.passengers,
                films_count: result.films.length,
                films: result.films
            };
            filmsAppeared = _.union(filmsAppeared, result.films);
        });

        getFilms(filmsAppeared, function(filmArr) {
            json.result.forEach((res, index) => {
                res.films.forEach((film, ind) => {
                    json.result[index].films[ind] = filmArr[film];
                });
            });
            callback(json);
        });

    });
}

function outputHtml(elem, results, stared) {
    let app = document.querySelector(elem),
        container = document.createElement("div");
    container.classList.add('container');
    app.appendChild(container);

    results.forEach((result, index) => {

        let shipContainer = document.createElement("div");
        shipContainer.classList.add('ship');
        if (result.films_count === stared) {
            shipContainer.classList.add('ship-stared');
        }

        let shipTitle = document.createElement("h2");
        shipTitle.classList.add('ship-title');
        shipTitle.innerText = result.name;

        let shipModel = document.createElement("span");
        shipModel.classList.add('ship-model');
        shipModel.innerText = result.model;

        let shipFilms = document.createElement("span");
        shipFilms.classList.add('ship-films');
        shipFilms.innerText = result.films_count;

        shipTitle.appendChild(shipFilms);
        shipContainer.appendChild(shipTitle);
        shipContainer.appendChild(shipModel);

        container.appendChild(shipContainer);
    });

}

function app(elem) {
    formatJson(function(json) {
        console.log('------------ OUTPUT JSON ------------');
        console.log(JSON.stringify(json));
        sortOrder(json, function(json) {
            console.log('------------ OUTPUT SORTED JSON ------------');
            console.log(JSON.stringify(json));
            let highestNumberFilms = _.max(json.result, function(o) {
                return o.films_count;
            }), starFilmsNo = highestNumberFilms.films_count;
            outputHtml(elem, json.result, starFilmsNo);
        });
    });
}

app('[data-app]');